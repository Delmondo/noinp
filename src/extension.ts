import * as vscode from 'vscode';

var _disposables: Map<string, vscode.Disposable> = new Map<string, vscode.Disposable>();

export const BLANK_LCONFIG: vscode.LanguageConfiguration = {
	indentationRules:
	{
		decreaseIndentPattern: /(?!)/,
		increaseIndentPattern: /(?!)/
	},
	onEnterRules:
	[{
		beforeText: /.*/,
		afterText: /.*/,
		action: { indentAction: vscode.IndentAction.None }
	}]
};

export function removeRulesForLangId(lang: string)
{
	let disposable = vscode.languages.setLanguageConfiguration(
		lang, BLANK_LCONFIG);
	
	_disposables.set(lang, disposable);
}

export function removeRulesForDocument(document: vscode.TextDocument)
{
	if (document) {
		removeRulesForLangId(document.languageId);
	}
}

export function removeRulesForAllLanguages()
{
	vscode.languages.getLanguages().then(languages => {
		for (const lang of languages) {
			removeRulesForLangId(lang);
		}
	});
}

export function removeRulesForAllVisibleTextEditors()
{
	for (const t of vscode.window.visibleTextEditors) {
		if (t.document && !_disposables.has(t.document.languageId)) {
			removeRulesForLangId(t.document.languageId);
		}
	}
}

var _activated: boolean = false;

export function activate(context: vscode.ExtensionContext)
{
	if (!_activated) {
		removeRulesForAllLanguages();
		_activated = true;
	}

	vscode.window.onDidChangeVisibleTextEditors(textEditors => {
		for (const t of textEditors) {
			if (t.document && !_disposables.has(t.document.languageId)) {
				removeRulesForLangId(t.document.languageId);
			}
		}
	});

// extension could add new language
	vscode.extensions.onDidChange(() => {
		removeRulesForAllLanguages();
	});
}

export function deactivate()
{
	for (const d of _disposables.values()) {
		d.dispose();
	}

	_activated = false;
}